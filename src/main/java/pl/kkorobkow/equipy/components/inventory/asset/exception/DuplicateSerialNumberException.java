package pl.kkorobkow.equipy.components.inventory.asset.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Inny zasób z takim numerem seryjnym już istnieje")
public class DuplicateSerialNumberException extends RuntimeException {
}
