package pl.kkorobkow.equipy.components.inventory.asset;

import org.springframework.stereotype.Service;
import pl.kkorobkow.equipy.components.inventory.asset.dto.AssetAssignmentDto;
import pl.kkorobkow.equipy.components.inventory.asset.dto.AssetDto;
import pl.kkorobkow.equipy.components.inventory.asset.exception.AssetNotFoundException;
import pl.kkorobkow.equipy.components.inventory.asset.exception.DuplicateSerialNumberException;
import pl.kkorobkow.equipy.components.inventory.asset.mapper.AssetAssignmentMapper;
import pl.kkorobkow.equipy.components.inventory.asset.mapper.AssetMapper;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
class AssetService {
    private final AssetRepository assetRepository;
    private final AssetMapper assetMapper;

    AssetService(AssetRepository assetRepository, AssetMapper assetMapper) {
        this.assetRepository = assetRepository;
        this.assetMapper = assetMapper;
    }

    List<AssetDto> findAll() {
        List<Asset> list = assetRepository.findAll();
        return mapAssetsListToDtoList(list);
    }

    List<AssetDto> findAllByNameOrSerialNumber(String text) {
        List<Asset> list = assetRepository.findAllByNameOrSerialNumber(text);
        return mapAssetsListToDtoList(list);
    }

    private List<AssetDto> mapAssetsListToDtoList(List<Asset> list) {
        return list.stream()
                .map(assetMapper::toDto)
                .collect(Collectors.toList());
    }

    Optional<AssetDto> findById(Long id) {
        return assetRepository.findById(id).map(assetMapper::toDto);
    }

    AssetDto save(AssetDto asset) {
        Optional<Asset> assetBySerialNumber = assetRepository.findBySerialNumber(asset.getSerialNumber());
        assetBySerialNumber.ifPresent(a -> {
            throw new DuplicateSerialNumberException();
        });
        return mapAndSaveAsset(asset);
    }


    AssetDto update(AssetDto asset) {
        Optional<Asset> assetBySerialNumber = assetRepository.findBySerialNumber(asset.getSerialNumber());
        assetBySerialNumber.ifPresent(a -> {
            if (!a.getId().equals(asset.getId()))
                throw new DuplicateSerialNumberException();
        });
        return mapAndSaveAsset(asset);
    }

    private AssetDto mapAndSaveAsset(AssetDto asset) {
        Asset assetEntity = assetMapper.toEntity(asset);
        Asset savedAsset = assetRepository.save(assetEntity);
        return assetMapper.toDto(savedAsset);
    }

    List<AssetAssignmentDto> getAssetAssignments(Long assetId) {
        return assetRepository.findById(assetId)
                .map(Asset::getAssignments)
                .orElseThrow(AssetNotFoundException::new)
                .stream()
                .map(AssetAssignmentMapper::toDto)
                .collect(Collectors.toList());
    }
}
