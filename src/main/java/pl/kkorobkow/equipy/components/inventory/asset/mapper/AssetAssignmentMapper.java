package pl.kkorobkow.equipy.components.inventory.asset.mapper;

import pl.kkorobkow.equipy.components.assignment.Assignment;
import pl.kkorobkow.equipy.components.inventory.asset.dto.AssetAssignmentDto;
import pl.kkorobkow.equipy.components.user.User;

public class AssetAssignmentMapper {
    public static AssetAssignmentDto toDto(Assignment entity) {
        AssetAssignmentDto dto = new AssetAssignmentDto();
        dto.setId(entity.getId());
        dto.setStart(entity.getStart());
        dto.setEnd(entity.getEnd());
        User user = entity.getUser();
        dto.setUserId(user.getId());
        dto.setFirstName(user.getFirstName());
        dto.setLastName(user.getLastName());
        dto.setPesel(user.getPesel());
        return dto;
    }
}
