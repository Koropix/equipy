package pl.kkorobkow.equipy.components.assignment.exception;

public class InvalidAssignmentException extends RuntimeException {
    public InvalidAssignmentException(String message) {
        super(message);
    }
}
