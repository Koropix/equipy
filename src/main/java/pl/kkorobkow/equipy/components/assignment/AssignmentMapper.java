package pl.kkorobkow.equipy.components.assignment;

import org.springframework.stereotype.Controller;
import pl.kkorobkow.equipy.components.inventory.asset.Asset;
import pl.kkorobkow.equipy.components.inventory.asset.AssetRepository;
import pl.kkorobkow.equipy.components.user.User;
import pl.kkorobkow.equipy.components.user.UserRepository;

import java.util.Optional;

@Controller
class AssignmentMapper {
    private final UserRepository userRepository;
    private final AssetRepository assetRepository;

    public AssignmentMapper(UserRepository userRepository, AssetRepository assetRepository) {
        this.userRepository = userRepository;
        this.assetRepository = assetRepository;
    }

    AssignmentDto toDto(Assignment entity) {
        AssignmentDto dto = new AssignmentDto();
        dto.setId(entity.getId());
        dto.setStart(entity.getStart());
        dto.setEnd(entity.getEnd());
        dto.setUserId(entity.getUser().getId());
        dto.setAssetId(entity.getAsset().getId());
        return dto;
    }

    Assignment toEntity(AssignmentDto dto) {
        Assignment entity = new Assignment();
        entity.setId(dto.getId());
        entity.setStart(dto.getStart());
        if (dto.getEnd() != null)
            entity.setEnd(dto.getEnd());
        Optional<User> user = userRepository.findById(dto.getUserId());
        user.ifPresent(entity::setUser);
        Optional<Asset> asset = assetRepository.findById(dto.getAssetId());
        asset.ifPresent(entity::setAsset);
        return entity;
    }
}
