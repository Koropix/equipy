package pl.kkorobkow.equipy.components.assignment;

import org.springframework.stereotype.Service;
import pl.kkorobkow.equipy.components.assignment.exception.AssignmentAlreadyFinishedException;
import pl.kkorobkow.equipy.components.assignment.exception.AssignmentNotFoundException;
import pl.kkorobkow.equipy.components.assignment.exception.InvalidAssignmentException;
import pl.kkorobkow.equipy.components.inventory.asset.Asset;
import pl.kkorobkow.equipy.components.inventory.asset.AssetRepository;
import pl.kkorobkow.equipy.components.user.User;
import pl.kkorobkow.equipy.components.user.UserRepository;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Optional;

@Service
class AssignmentService {
    private final AssignmentRepository assignmentRepo;
    private final UserRepository userRepo;
    private final AssetRepository assetRepo;
    private final AssignmentMapper assignmentMp;

    AssignmentService(AssignmentRepository assignmentRepo, UserRepository userRepo, AssetRepository assetRepo, AssignmentMapper assignmentMp) {
        this.assignmentRepo = assignmentRepo;
        this.userRepo = userRepo;
        this.assetRepo = assetRepo;
        this.assignmentMp = assignmentMp;
    }

    AssignmentDto save(AssignmentDto assignmentDto) {
        Optional<Assignment> activeAssignmentForAsset = assignmentRepo.findByAsset_IdAndEndIsNull(assignmentDto.getAssetId());
        activeAssignmentForAsset.ifPresent(a -> {
            throw new InvalidAssignmentException("Zasób o ID " + a.getAsset().getId() + " jest już wypożyczony");
        });
        Assignment assignment = new Assignment();
        userIfPresentOrElseThrow(assignmentDto.getUserId(), assignment);
        assetIfPresentOrElseThrow(assignmentDto.getAssetId(), assignment);
        assignment.setStart(LocalDateTime.now());
        Assignment savedAssignment = assignmentRepo.save(assignment);
        return assignmentMp.toDto(savedAssignment);
    }

    private void userIfPresentOrElseThrow(Long userId, Assignment assignment) {
        Optional<User> user = userRepo.findById(userId);
        user.ifPresentOrElse(assignment::setUser, () -> {
            throw new InvalidAssignmentException("Nie odnaleziono użytkownika o ID: " + userId);
        });
    }

    private void assetIfPresentOrElseThrow(Long assetId, Assignment assignment) {
        Optional<Asset> asset = assetRepo.findById(assetId);
        asset.ifPresentOrElse(assignment::setAsset, () -> {
            throw new InvalidAssignmentException("Nie odnaleziono zasobu o ID: " + assetId);
        });
    }

    @Transactional
    public LocalDateTime finishAssignment(Long assignmentId) {
        Optional<Assignment> assignment = assignmentRepo.findById(assignmentId);
        Assignment assignmentEntity = assignment.orElseThrow(AssignmentNotFoundException::new);
        if (assignmentEntity.getEnd() != null)
            throw new AssignmentAlreadyFinishedException();
        assignmentEntity.setEnd(LocalDateTime.now());
        return assignmentEntity.getEnd();
    }

//    LocalDateTime end(Long assignmentId) {
//        LocalDateTime returnTime = LocalDateTime.now();
//        Optional<Assignment> assignment = assignmentRepo.findById(assignmentId);
//        assignment.ifPresentOrElse(a -> {
//            if(isClosable(a)) {
//                a.setEnd(returnTime);
//                assignmentRepo.save(a);
//            }
//        }, () -> {
//            throw new AssignmentNotFoundException();
//        });
//        return returnTime;
//    }
//
//    private boolean isClosable(Assignment assignment) {
//        if (assignment.getEnd() != null)
//            throw new AssignmentAlreadyFinishedException();
//        return true;
//    }
}
