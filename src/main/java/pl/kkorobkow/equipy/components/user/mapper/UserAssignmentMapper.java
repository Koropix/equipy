package pl.kkorobkow.equipy.components.user.mapper;

import pl.kkorobkow.equipy.components.assignment.Assignment;
import pl.kkorobkow.equipy.components.inventory.asset.Asset;
import pl.kkorobkow.equipy.components.user.dto.UserAssignmentDto;

public class UserAssignmentMapper {

    public static UserAssignmentDto toDto(Assignment entity) {
        UserAssignmentDto dto = new UserAssignmentDto();
        dto.setId(entity.getId());
        dto.setStart(entity.getStart());
        dto.setEnd(entity.getEnd());
        Asset asset = entity.getAsset();
        dto.setAssetId(asset.getId());
        dto.setAssetName(asset.getName());
        dto.setAssetSerialNumber(asset.getSerialNumber());
        return dto;
    }
}
